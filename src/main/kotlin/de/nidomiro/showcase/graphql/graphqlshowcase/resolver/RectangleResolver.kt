package de.nidomiro.showcase.graphql.graphqlshowcase.resolver

import de.nidomiro.showcase.graphql.graphqlshowcase.RectangleDto
import graphql.kickstart.tools.GraphQLQueryResolver
import org.jboss.logging.Logger
import org.springframework.stereotype.Service


@Service
@Suppress("unused")
class RectangleResolver : GraphQLQueryResolver {

    fun getRectangle(input: RectangleDto): RectangleDto {
        logger.info("Called getRectangle")
        return RectangleDto(input.position, input.width, input.height)
    }


    companion object {
        private val logger = Logger.getLogger(RectangleResolver::class.java)
    }
}