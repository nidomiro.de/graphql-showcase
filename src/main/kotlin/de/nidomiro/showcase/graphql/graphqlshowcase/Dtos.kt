package de.nidomiro.showcase.graphql.graphqlshowcase

data class Point2DDto(
    val x: Double,
    val y: Double
)

data class RectangleDto(
    val position: Point2DDto,
    val width: Double,
    val height: Double
)