package de.nidomiro.showcase.graphql.graphqlshowcase.resolver


import de.nidomiro.showcase.graphql.graphqlshowcase.Point2DDto
import de.nidomiro.showcase.graphql.graphqlshowcase.service.Point2DService
import graphql.kickstart.tools.GraphQLMutationResolver
import graphql.kickstart.tools.GraphQLQueryResolver
import org.springframework.stereotype.Service


@Service
@Suppress("unused")
class SetGetPointResolver(
    private val point2DService: Point2DService
) : GraphQLQueryResolver, GraphQLMutationResolver {


    fun getPoint() = point2DService.savePoint

    fun setPoint(x: Double, y: Double): Point2DDto {
        point2DService.savePoint = Point2DDto(x, y)
        return point2DService.savePoint
    }
}