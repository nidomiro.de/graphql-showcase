package de.nidomiro.showcase.graphql.graphqlshowcase.resolver

import graphql.kickstart.tools.GraphQLSubscriptionResolver
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import org.reactivestreams.Publisher
import org.springframework.stereotype.Component
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.random.Random


@Component
class ExampleSubscriptionResolver: GraphQLSubscriptionResolver {

    private val random = Random(0)

    val publisher: Publisher<Int> by lazy {
        val randomEmittingObservable = Observable.create<Int> { emitter ->
            val executorService = Executors.newScheduledThreadPool(1)
            executorService.scheduleAtFixedRate( {
                val randNumber = random.nextInt()
                emitter.onNext(randNumber)
            }, 0, 2, TimeUnit.SECONDS)

        }

        val connectableObservable = randomEmittingObservable.share().publish()
        connectableObservable.connect()

        connectableObservable.toFlowable(BackpressureStrategy.BUFFER)
    }

    init {

    }

    /**
     * Usage:
     * ws://localhost:8080/subscriptions
     * {
     *  "query": "subscription Sub { result: randomNumbers }",
     *  "vaiables": "{}"
     * }
     */
    @Suppress("unused")
    fun randomNumbers(): Publisher<Int> {
        return publisher
    }
}