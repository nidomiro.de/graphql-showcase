package de.nidomiro.showcase.graphql.graphqlshowcase.resolver

import graphql.kickstart.tools.GraphQLQueryResolver
import org.springframework.stereotype.Service

@Service
@Suppress("unused")
class HelloResolver : GraphQLQueryResolver {

    fun hello(name: String) = "Hello $name"
}