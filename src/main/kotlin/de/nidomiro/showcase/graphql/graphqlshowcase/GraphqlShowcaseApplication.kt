package de.nidomiro.showcase.graphql.graphqlshowcase

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GraphqlShowcaseApplication

fun main(args: Array<String>) {
    runApplication<GraphqlShowcaseApplication>(*args)
}
