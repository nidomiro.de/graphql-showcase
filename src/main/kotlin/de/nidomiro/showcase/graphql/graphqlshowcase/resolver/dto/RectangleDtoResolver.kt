package de.nidomiro.showcase.graphql.graphqlshowcase.resolver.dto

import de.nidomiro.showcase.graphql.graphqlshowcase.RectangleDto
import graphql.kickstart.tools.GraphQLResolver
import org.jboss.logging.Logger
import org.springframework.stereotype.Service


@Service
@Suppress("unused")
class RectangleDtoResolver : GraphQLResolver<RectangleDto> {


    fun area(rectangleDto: RectangleDto): Double {
        logger.info("Called area-function")

        return rectangleDto.width * rectangleDto.height
    }


    companion object {
        private val logger = Logger.getLogger(RectangleDtoResolver::class.java)
    }
}