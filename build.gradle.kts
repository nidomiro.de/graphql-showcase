import org.jetbrains.kotlin.gradle.tasks.KotlinCompile




plugins {

	val kotlinVersion: String? = "1.3.71"


	id("org.springframework.boot") version "2.1.5.RELEASE"
	id("io.spring.dependency-management") version "1.0.7.RELEASE"
	kotlin("jvm") version kotlinVersion
	kotlin("plugin.spring") version kotlinVersion
}

group = "de.nidomiro.showcase.graphql"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")

	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")


	implementation("com.graphql-java-kickstart:graphql-spring-boot-starter:7.1.0")

	compile("io.reactivex.rxjava2:rxjava:2.2.19")

	runtimeOnly("com.graphql-java-kickstart:playground-spring-boot-starter:7.1.0")

	// testing facilities
	testCompile("com.graphql-java-kickstart:graphql-spring-boot-starter-test:7.1.0")

	testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "1.8"
	}
}
